# Day-2 :
---------

### Topics of Day-2 hands-on session

- Structure optimizations
- NEB: transition states of elementary chemical reactions
- Functionals
- Automating the workflow with PWTK

---

**Exercise 1:** structural optimization (`calculation = 'relax'`);
	            atomic positions only

    cd example1.relax/


**Exercise 2:** structural optimization (`calculation = 'vc-relax'`);
                atomic positions and unitcell

    cd example2.vc-relax/


**Exercise 3:** transition states of elementary chemical reactions

    cd example3.neb/


**Exercise 4:** (advanced) functionals

    cd example4.functionals/


**Exercise 5:** automating the workflow with PWTK

    cd example5.pwtk/
