# Exercise 4

The QE package provides two "miniapps" to simplify the determination of the most effective parallel performances configurations.

## FFTXlib

In order to compile the FFT miniapp go back to the directory where you commpiled QE, enter the FFTXlib folder and `make TEST`,

    # Run me!
    cd /path/to/q-e
    cd FFTXlib
    make TEST

TODO: check if this actually works.

## LAXlib

Enter now the LAXlib directory and compile the miniapp with

    # Run me!
    cd /path/to/q-e
    cd LAXlib
    make TEST

TODO: check if this actually works.