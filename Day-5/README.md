# Day-5 :

### Morning

- Introduction to HPC environment
- Optimized QE configuration and compilation
- Run-time options: description and usage
- Miniapps: benchmarking made simple

### Afternoon

- Concise introduction to accelerated hardware details
- Optimized QE-GPU: configuration and compilation
- Run-time options for QE-GPU
- Advanced topics: CUDA-aware MPI, GPU oversubscription.

---

Exercise 1: check the environment

    cd exercise-1


Exercise 2: optimized QE configuration and compilation

    cd exercise-2

Exercise 3: Run-time options

    cd exercise-3


Exercise 4: miniapps

    cd exercise-4

Exercise 5: introduction to GPGPUs

    cd exercise-5

Exercise 6: compilation of QE-GPU

    cd exercise-6

Exercise 7: execution of QE-GPU

    cd exercise-7

Exercise 8: advanced options

    cd exercise-8
