# Day-3 :
---------

### Topics of Day-3 hands-on session

- Phonon exercises 
- TDDFPT exercises

-----------

**Exercise 1:** 
    
**Exercise 2:** 
    
**Exercise 3:** 

**Exercise 4:** Calculation of the absorption spectrum of benzene molecule (C6H6) using the Independent Particle Approximation

    cd example4/

**Exercise 5:** Calculation of the absorption spectrum of benzene molecule (C6H6) using the turbo_davidson.x code

    cd example5/

**Exercise 6:** Calculation of the absorption spectrum of benzene molecule (C6H6) using the turbo_lanczos.x code

    cd example6/

**Exercise 7:** Calculation of the absorption spectrum of methane molecule (CH4) with hybrid pseudo-potential using the turbo_davidson.x code

    cd example7/
